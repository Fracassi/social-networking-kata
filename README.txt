Prerequisiti:
* jdk 1.8 o superiori
* ant

Per compilare:
* il codice applicativo: ant compile
* i test: ant test-compile

Per eseguire i test (esegue anche la compilazione): ant test

Per eseguire il programma: java -cp ./out/application production.Main